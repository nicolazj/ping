var ping = require("./ping");
var async = require('async');

function lag(host, cb) {

  var sum = 0;
  var avg = 0;
  var responsive = false;
  var exec = ping(host, {
    count: 3
  }); // default 10 packets

  exec.on("data", function(data) {
    responsive = true;
    sum += data.time;
  });

  exec.on("exit", function(data) {
    if (responsive) {
      avg = sum / 3;
    } else {
      avg = -1;
    }
    cb(Number(avg).toFixed(0));
  });
}


module.exports = function(hosts, cb) {
  // var hosts = ['google.com', 'baidu.com', 'baidu.com'];
  var tasks = {};
  for (var i in hosts) {
    (function(i){
      var host = hosts[i];
      tasks[host] = function(callback) {
        if(/[A-Za-z0-9\.]+/.test(host)){
          lag(host, function(lag) {
            callback(null, lag);
          });
        }else{
    
          callback(null,-1);
        }

      }
    })(i)
  }

  async.series(tasks,
    function(err, results) {
      cb(results);
      // results is now equal to: {one: 1, two: 2}
    });
}
