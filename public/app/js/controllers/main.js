'use strict';

var controllersModule = require('./_index');

/**
 * @ngInject
 */
function MainCtrl( $mdSidenav) {

  // ViewModel
  var vm = this;

  vm.toggleSidenav = function(menuId) {
    $mdSidenav(menuId).toggle();
  };

}

controllersModule.controller('MainCtrl', MainCtrl);
