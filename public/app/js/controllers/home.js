'use strict';

var controllersModule = require('./_index');

/**
 * @ngInject
 */
function HomeCtrl($interval, $http,$scope) {

  // ViewModel
  var vm = this;


  vm.records = {
    'google.com':{},
    'bing.com':{}
  };

  vm.hostToAdd = '';

  vm.addHost = function() {
    vm.records[vm.hostToAdd] = {};
    vm.hostToAdd = '';
    $scope.form.$setPristine();
  };
  vm.removeHost = function(host) {
    delete vm.records[host];

  };

vm.count = 0;
  $interval(function() {
    if(vm.count!==0){
      vm.count-=1;
    }else{
      vm.count = 10;
      vm.loading = true;
      var req = {
        method: 'GET',
        url: '/ping',

        params: {
          hosts: Object.keys(vm.records).join(',')
        }
      }
      $http(req).
      success(function(data, status, headers, config) {
        // this callback will be called asynchronously
        // when the response is available
        for(var i in data){
          vm.records[i].lag = data[i]==-1?'No response':data[i]+'ms';
        }
        vm.loading = false;


      }).
      error(function(data, status, headers, config) {
        // called asynchronously if an error occurs
        // or server returns response with an error status.
      });
    }


  }, 1000);

}

controllersModule.controller('HomeCtrl', HomeCtrl);
